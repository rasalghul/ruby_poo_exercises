class SayHi
	def initialize
		@message = "Write your name: "
	end

	def ask
		puts @message
		name = gets.chomp
		puts ""
		puts "Hello! " + name
	end
end

hello = SayHi.new
hello.ask